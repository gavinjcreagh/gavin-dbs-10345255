angular.module('starter.services', [])


//Weather service - call api and populates an array with data
.factory('Weather', function($q,$http) {
  
  var weathers=[];

  return {
    load : function () {

      //declare api
      var deferred = $q.defer();
      var weatherUrl = "https://api.openweathermap.org/data/2.5/group?id=7778677,2965140,2962941,2964179&units=metric&appid=";
      var key = "e12f07c8d9437ef9fe18015f148b88b8";
      
      //get data from api
      $http.get(weatherUrl+key, {}).success(function (data) {

              
              //add weather objects to array
              angular.forEach(data.results, function(weather) {
                weathers.push(weather);
                });
              
                
                deferred.resolve();
        })
        .error( function () {

              alert('There was an error');

        });
              return weathers;
            },
            
    get: function(weatherId) {
        for (var i = 0; i < weathers.length; i++)
        {
            if (weathers[i].id === parseInt(weatherId))
            {
              return weathers[i];
            }
      }

      return null;

        },
        
        remove: function(weather) {
          weathers.splice(weathers.indexOf(weather), 1);
        }
    }
});


